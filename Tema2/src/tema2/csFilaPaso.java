
package tema2;

import javax.swing.JOptionPane;

public class csFilaPaso {
   
    private double x;
    private int i;
    private double y;
    

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "csFilaPaso{" + "x=" + x + ", i=" + i + ", y=" + y + '}';
    }



    
    
}

