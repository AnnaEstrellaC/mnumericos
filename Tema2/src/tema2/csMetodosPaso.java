
package tema2;

import java.util.ArrayList;
import javax.swing.JOptionPane;


public class csMetodosPaso {
    
    public ArrayList<csFilaPaso> Paso(double a,double b, double h){
        
       double indice=(a-b)/h;
       int n = 1+((int)indice);
       double incremento=0.0;
       ArrayList<csFilaPaso> lista;
        lista = new ArrayList<csFilaPaso>();
        int i=0;
        
        double resultado=1;
        while (i<n) {
            csFilaPaso fila = new csFilaPaso();
                      
            if (i==0) {
             fila.setI(i);
             fila.setX(incremento);
             fila.setY(resultado);
                        

            } else {
             fila.setI(i); // imprime posicion
             fila.setY(resultado+(h*this.funcion(incremento))); 
             resultado=+fila.getY();
             incremento=incremento+h;
             fila.setX(incremento);
            }
            lista.add(fila);
            i++;
        }
       return lista;
        
    }

    
    public double funcion(double x){
        return (-2*Math.pow(x,3))+(12*Math.pow(x,2))-(20*x)+(8.5);
    }
    
    
}
