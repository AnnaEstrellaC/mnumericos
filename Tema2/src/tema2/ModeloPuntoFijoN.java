
package tema2;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class ModeloPuntoFijoN extends AbstractTableModel{

    private String []columnas={"I", "X1", "X2","X3", "Error"};
    private ArrayList<csFilaPuntoFijoN> lista;
    
        public ModeloPuntoFijoN() {
        
    }
        public ModeloPuntoFijoN(ArrayList<csFilaPuntoFijoN> lista) {
        this.lista = lista;
    }
        
    
    @Override
    public int getRowCount() {
        return this.lista.size();
//  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        return this.columnas.length;
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int fila, int columna) {
       switch(columna){
        case 0: return this.lista.get(fila).getI();
        case 1: return this.lista.get(fila).getX1();
        case 2: return this.lista.get(fila).getX2();
        case 3: return this.lista.get(fila).getX3();
        case 4: return this.lista.get(fila).getError();
        default: return null;
       }
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public String getColumnName(int i) {
        //return super.getColumnName(i); //To change body of generated methods, choose Tools | Templates.
      return this.columnas[i];
    }
    
}
