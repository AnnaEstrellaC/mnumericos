package tema2;

import java.util.ArrayList;

public class csMetodoPuntoFijoN {
 
    public ArrayList<csFilaPuntoFijoN> Punto(double x1, double x2,double x3,double ErrorDeseado ){
        double AnteriorX1=x1;
        double AnteriorX2=x2;
        double AnteriorX3=x3;
        
        double Error;
        
        ArrayList<csFilaPuntoFijoN> lista;
        
         int i = 1; //Contador
        lista = new ArrayList<csFilaPuntoFijoN>();
        
        do{
         csFilaPuntoFijoN fila = new csFilaPuntoFijoN();
         
         
            fila.setI(i);
            fila.setX1(this.MetodoX1(AnteriorX2, AnteriorX3));
            fila.setX2(this.MetodoX2(AnteriorX1, AnteriorX3));
            fila.setX3(this.MetodoX3(AnteriorX1, AnteriorX2));
            
            double ErrorX1,ErrorX2,ErrorX3;
            ErrorX1=Math.abs(AnteriorX1-fila.getX1());
            ErrorX2=Math.abs(AnteriorX2-fila.getX2());
            ErrorX3=Math.abs(AnteriorX3-fila.getX3());
            
            Double Maximo=Math.max(ErrorX1, ErrorX2);
            fila.setError(Math.max(Maximo, ErrorX3));
            Error=fila.getError();
            
            AnteriorX1=fila.getX1();
            AnteriorX2=fila.getX2();
            AnteriorX3=fila.getX3();
            
            
          //  System.out.println(fila.toString());
          lista.add(fila);
            fila.setI(i++); 
        }while(Error>ErrorDeseado);
        
        return lista;
            
            }
    double a;
    public double MetodoX1(double X2,double X3){
        double b=X2*X3;
     a=((Math.cos(b))/3)+0.1666666667;
    return a;
}
    public double MetodoX2(double X1,Double X3){
        double x=Math.sin(X3);
        double y=Math.pow(X1, 2);
        a=(Math.sqrt(x+y+1.06));
        double w=a*(0.1111111111);
        double z=w-0.1;
        return z;
    }
    public double MetodoX3(double X1,double X2){
        double b=(-1)*(X1*X2);
        a=((Math.pow(Math.E,b)*-1)/20)-(((10*Math.PI)-3)/60);
        return a;
    }
}

