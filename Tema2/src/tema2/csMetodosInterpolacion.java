
package tema2;

import java.util.ArrayList;


public class csMetodosInterpolacion {
    
   public double MetodoInterNewton(double a[][], double x){
       
		
		double y,fx1x0,fx2x1,fx3x2,fx4x3,fx2x1x0,fx3x2x1,fx4x3x2,fx3x2x1x0,fx4x3x2x1,fx4x3x2x1x0;
		csFilaInterpolacion i=new csFilaInterpolacion();
				
		fx1x0=(a[1][1]-a[0][1])/(a[1][0]-a[0][0]);
                System.out.println(i.getD11());
                fx2x1=(a[2][1]-a[1][1])/(a[2][0]-a[1][0]);
                i.setD12(fx2x1);
		fx3x2=(a[3][1]-a[2][1])/(a[3][0]-a[2][0]);
                i.setD13(fx3x2);
		fx4x3=(a[4][1]-a[3][1])/(a[4][0]-a[3][0]);
                i.setD14(fx4x3);
                
		fx2x1x0=(fx2x1-fx1x0)/(a[2][0]-a[0][0]);
                i.setD21(fx2x1x0);
		fx3x2x1=(fx3x2-fx2x1)/(a[3][0]-a[1][0]);
                i.setD22(fx3x2x1);
		fx4x3x2=(fx4x3-fx3x2)/(a[4][0]-a[2][0]);
                i.setD23(fx4x3x2);
                
		fx3x2x1x0=(fx3x2x1-fx2x1x0)/(a[3][0]-a[0][0]);
                i.setD31(fx3x2x1x0);
		fx4x3x2x1=(fx4x3x2-fx3x2x1)/(a[3][0]-a[0][0]);
                i.setD32(fx4x3x2x1);
                
		fx4x3x2x1x0=(fx4x3x2x1-fx3x2x1x0)/(a[4][0]-a[0][0]);
                i.setD41(fx4x3x2x1x0);
                
		y=a[0][1]+fx1x0*(x-a[0][0])+fx2x1x0*(x-a[0][0])*(x-a[1][0])+fx3x2x1x0*(x-a[0][0])*(x-a[1][0])*(x-a[2][0])+fx4x3x2x1x0*(x-a[0][0])*(x-a[1][0])*(x-a[2][0])*(x-a[3][0]);
	      
              
              return y;
		}

}
