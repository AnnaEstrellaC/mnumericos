
package tema2;

public class csFilaPuntoFijoN {
 
    private double x1,x2,x3,Error;
    private int i;

    @Override
    public String toString() {
        return "csFilaPuntoFijoN{" + "x1=" + x1 + ", x2=" + x2 + ", x3=" + x3 + ", Error=" + Error + ", i=" + i + '}';
    }
    
    
    

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
    
    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getX2() {
        return x2;
    }

    public void setX2(double x2) {
        this.x2 = x2;
    }

    public double getX3() {
        return x3;
    }

    public void setX3(double x3) {
        this.x3 = x3;
    }

    public double getError() {
        return Error;
    }

    public void setError(double Error) {
        this.Error = Error;
    }
    
    
}
