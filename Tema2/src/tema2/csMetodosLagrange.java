
package tema2;

import java.util.ArrayList;

public class csMetodosLagrange {
    
   public double Lagrange(double a[][],double x){
        
        double y;
        

                y =     ((x - a[1][0]) * (x - a[3][0]) * (x - a[2][0]) * a[0][1]) /((a[0][0] - a[1][0]) * (a[0][0] - a[2][0]) * (a[0][0] - a[3][0])) 
                        + ((x - a[0][0]) * (x - a[2][0]) * (x - a[3][0]) * a[1][1]) /((a[1][0] - a[0][0]) * (a[1][0] - a[2][0]) * (a[1][0] - a[3][0])) 
                        + ((x - a[0][0]) * (x - a[1][0]) * ((x - a[3][0]) * a[2][1]) / ((a[2][0] - a[0][0]) * (a[2][0] - a[1][0]) * (a[2][0] - a[3][0])) 
                        + ((x - a[0][0]) * (x - a[1][0]) * ((x - a[2][0]) * a[3][1]) / ((a[3][0] - a[0][0]) * (a[3][0] - a[1][0]) * (a[3][0] - a[2][0]))));
                
      return y;         
    }
   
}
