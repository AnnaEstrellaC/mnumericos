
package tema2;

public class csFilaInterpolacion {
    
    private double d11,d12,d13,d14,d21,d22,d23,d31,d32,d41;

    public double getD11() {
        return d11;
    }

    public void setD11(double d11) {
        this.d11 = d11;
    }

    public double getD12() {
        return d12;
    }

    public void setD12(double d12) {
        this.d12 = d12;
    }

    public double getD13() {
        return d13;
    }

    public void setD13(double d13) {
        this.d13 = d13;
    }

    public double getD14() {
        return d14;
    }

    public void setD14(double d14) {
        this.d14 = d14;
    }

    public double getD21() {
        return d21;
    }

    public void setD21(double d21) {
        this.d21 = d21;
    }

    public double getD22() {
        return d22;
    }

    public void setD22(double d22) {
        this.d22 = d22;
    }

    public double getD23() {
        return d23;
    }

    public void setD23(double d23) {
        this.d23 = d23;
    }

    public double getD31() {
        return d31;
    }

    public void setD31(double d31) {
        this.d31 = d31;
    }

    public double getD32() {
        return d32;
    }

    public void setD32(double d32) {
        this.d32 = d32;
    }

    public double getD41() {
        return d41;
    }

    public void setD41(double d41) {
        this.d41 = d41;
    }

    @Override
    public String toString() {
        return "csFilaInterpolacion{" + "d11=" + d11 + ", d12=" + d12 + ", d13=" + d13 + ", d14=" + d14 + ", d21=" + d21 + ", d22=" + d22 + ", d23=" + d23 + ", d31=" + d31 + ", d32=" + d32 + ", d41=" + d41 + '}';
    }
    
       
    
    
}
